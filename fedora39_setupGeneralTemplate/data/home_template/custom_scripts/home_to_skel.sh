#!/bin/bash
#Call this in template VM to write the home folder into /dev/skel
#this is then used to generate appvms home folders
# https://www.qubes-os.org/doc/templates/
sudo rm -rf /etc/skel
sudo cp -ra ~ /etc/skel
sudo chmod 755 /etc/skel

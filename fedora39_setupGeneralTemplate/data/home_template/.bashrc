# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=


# User specific aliases and functions
PATHSCRIPTS="/home/user/custom_scripts/"
alias luksOpen="bash ${PATHSCRIPTS}luksOpen.sh"
alias luksClose="bash ${PATHSCRIPTS}luksClose.sh"
alias luksNew="bash ${PATHSCRIPTS}luksNew.sh"

alias gedit="gedit --new-window"

# python autoimports
export PYTHONSTARTUP="/home/user/.ipython/profile_default/startup/00-all.py"

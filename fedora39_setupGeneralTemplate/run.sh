#!/bin/bash

# ADJUST THIS SECTION
# ============================================================================================================
DOWNLOADLINK_PYCHARM="https://download.jetbrains.com/python/pycharm-community-2023.3.3.tar.gz"
# ============================================================================================================


set -e  # exit on error
shopt -s extglob  # pattern list matching, https://www.linuxjournal.com/content/bash-extended-globbing
set -o xtrace  # print every command before execution



FOLDER_DATA="data"
FOLDER_PREPARED="tmp_prepared"
FOLDER_HOME_TEMPLATE="/etc/skel"  # https://www.qubes-os.org/doc/templates/
FOLDER_HOME="/home/user"



install_pycharm () {
  if ls ${FOLDER_PREPARED}/*pycharm-community* 1> /dev/null 2>&1; then
    echo "pycharm already downloaded"
  else
    echo "downloading pycharm"
    wget ${DOWNLOADLINK_PYCHARM} -P ${FOLDER_PREPARED}
  fi
  
  # extract
  sudo rm -rf /opt/*pycharm*
  sudo tar -xf ${FOLDER_PREPARED}/*pycharm*.tar.gz --directory /opt
  
  # create alias
  EXEC_PYCHARM=(`ls -d /opt/*pycharm-community*/bin/pycharm.sh`)
  if [[ ! ${#EXEC_PYCHARM[@]} -eq 1 ]]
    then
      echo "Multiple candidates for pycharm executable: ${EXEC_PYCHARM[@]}"
      exit 1
    fi
  echo "alias pycharm=\"bash ${EXEC_PYCHARM[0]}\"" | sudo tee -a "${FOLDER_HOME_TEMPLATE}/.bashrc"
  echo "alias pycharm=\"bash ${EXEC_PYCHARM[0]}\"" | sudo tee -a "${FOLDER_HOME}/.bashrc"
}

available_hdd () {
  df -h
}

copy_home_folder () {
  sudo cp -r ${FOLDER_DATA}/home_template/. ${FOLDER_HOME_TEMPLATE}
  cp -r ${FOLDER_DATA}/home_template/.* ${FOLDER_HOME}/
  echo "copied home folder"
}

copy_fonts () {
  sudo cp -rf ${FOLDER_DATA}/fonts/* /usr/share/fonts/ | true  # | true: ignore if font folder is empty
  echo "copied fonts"
}

install_via_dnf () {
  sudo dnf install -y htop gedit wget g++ gparted  # sys tools
  sudo dnf install -y okular xournal ghostwriter  # pdf, markdown
  sudo dnf install -y audacious # audio player, eventually install some more audacius-* packages
  sudo dnf install -y inkscape gimp # image processing
  sudo dnf install -y libreoffice aspell-de hunspell-de  # office + spell checking
  sudo dnf install -y texlive-scheme-full
  
  

}

install_python () {
  # install pyhton packages. Per default pip installs into home folder.
  # This does not make sense for a templeate VM. Hence we install into /usr/lib/python*/site-packages
  sudo dnf install -y python3 python3-pip
  sudo pip install --upgrade numpy --target=/usr/lib64/python3.10/site-packages/  # upgrade preinstalled numpy
  PIP_TARGET=(`ls -d /usr/lib/python3*/site-packages`)  # array of len 1, example (/usr/lib/python3.10/site-packages)
  if [[ ${#PIP_TARGET[@]} -eq 1 ]]
  then
    echo "PIP_TARGET: ${PIP_TARGET[0]}"
    FOLDER_PIP_TMP="/var/tmp"  # part of system storage
    #sudo TMPDIR=tmp pip install -r ${FOLDER_DATA}/python_requirements_basics.txt --cache-dir=tmp --target="${PIP_TARGET[0]}"
    #TMPDIR=tmp pip install -r ${FOLDER_DATA}/python_requirements_ml.txt --cache-dir=tmp --build tmp --target="${PIP_TARGET[0]}"
    sudo TMPDIR=$FOLDER_PIP_TMP pip install --upgrade --force-reinstall -r data/python_requirements.txt --cache-dir=$FOLDER_PIP_TMP --target="${PIP_TARGET[0]}"
  else
    echo "Can not run pip install. Found multiple target directories: ${PIP_TARGET[@]}"
  fi
  pip freeze
}

configure_dconf () {
  # do changes manually and use
  # $ dconf watch /
  # to monitor the adjustments you need to make in this script.
  
  # nautilus: adjust view settings
  dconf write /org/gtk/gtk4/settings/file-chooser/show-hidden true
  dconf write /org/gnome/nautilus/preferences/default-folder-viewer "'list-view'"
  dconf write /org/gnome/nautilus/list-view/default-zoom-level "'small'"

  # terminal
  dconf write /org/gnome/terminal/legacy/profiles:/:$(gsettings get org.gnome.Terminal.ProfilesList default  | tr -d "'")/use-system-font false
  dconf write /org/gnome/terminal/legacy/keybindings/reset-and-clear "'<Primary>w'"
  
  # copy config file
  sudo mkdir -p ${FOLDER_HOME_TEMPLATE}/.config/dconf
  sudo cp -r /home/user/.config/dconf/user ${FOLDER_HOME_TEMPLATE}/.config/dconf/user
}


# script execution
if [ $# -eq 1 ]
then
    if [[ ! ${1} =~ ^(install|dev)$ ]]
    then
      echo "wrong argument \"${1}\". Call this script with either \"bash run.sh perpare\" or \"bash run.sh install\""
      exit 1
    fi
    if [[ ${1} == "dev" ]]  # for development purposes only
    then
      echo "Dev mode: just for development of this script."
      install_pycharm
    fi
    if [[ ${1} == "install" ]]
    then
      echo "Install mode: setting up a template VM (run this in the template VM)"
      sudo dnf upgrade -y
      copy_home_folder
      copy_fonts
      available_hdd
      install_via_dnf
      available_hdd
      install_python
      available_hdd
      install_pycharm
      available_hdd
      configure_dconf
      available_hdd
      echo "finished setting up template VM"
    fi
else
  echo "command line argument missing. Call this script with either \"bash run.sh perpare\" or \"bash run.sh install\"."
  exit 1
fi




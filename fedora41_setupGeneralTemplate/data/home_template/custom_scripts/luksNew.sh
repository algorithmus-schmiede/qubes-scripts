#!/bin/bash

NAME=$1
#NAME=finance
#SIZE=70
#read -p 'Conatiner Name: ' NAME
read -p 'Size (MB): ' SIZE

dd if=/dev/urandom of=${NAME}.crypt bs=1M count=${SIZE}
sudo cryptsetup -c aes-xts-plain64 -s 512 -h sha512 -y luksFormat ${NAME}.crypt

echo 'cryptsetup luksOpen ...'
sudo cryptsetup luksOpen ${NAME}.crypt tmpCont
echo 'mkfs.ntfs ...'
sudo mkfs.ntfs /dev/mapper/tmpCont -f -L ${NAME}
echo 'cryptsetup luksClose ...'
sudo cryptsetup luksClose tmpCont

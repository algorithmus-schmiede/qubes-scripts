# How to setup a new qubes template

- ```dom0: sudo qubes-dom0-update qubes-template-fedora-41```
- clone fedoraXX template to fXX
- update fXX from Qubes Manager or by ```sudo dnf update -y```
- adjust hdd space for fXX: private: TODO, system: TODO
- copy fedora... folder to fXX template
- fXX: ```cd QubesIncomming/fedora...; bash /run.sh install```

# What is done

Manual of how to setting up a new qubes template with some standrad packages installed and a few common configurations adjusted.

The following is done:
- upgrades all system packages
- install common linux tools (htop, gparted, some pdf viewers, ...)
- install inkscape + gimp
- install libreoffice + german spellchecking
- install latex
- install python with a large custom set of libraries
- install pycharm
- add files to default home directory created if new VMs are based on this template
- install custom fonts

# Preconditions

- Install template VM based on Fedora 41 by the following command on dom0

```
	sudo qubes-dom0-update qubes-template-fedora-41
```

- private storage: 2 GB
- system storage: 18 GB
- internet access (for pip install commands)
- for eu keyboard layout to work, use keyboard layout "English (US) English (US, euro on 5)" in dom0.

Assigning more storage to the TemplateVM as the minimal, does not lead to increased backup size. But never the less, storage can not be decreased in the qubes manager.

# History and ToDos

Releases are always called according to merge request (MR) / commit / branch name in gitlab repo. 

## ToDos
- [ ] dnf install open-cv (otherwise only python bindings are installed I guess. Test if opencv lib can be used from python)
- [ ] copy fonts to /usr/share/fonts as well, for existing App VMs
- [ ] switch from fedora-39 to fedora-39-xfce templates (more lightweight)
- [ ] pip install openpyxl (for gen invoices)
- [ ] dnf install ffmpeg-free (for vosk speech->text)
- [ ] implement in python and ensure beforehand that python works (upgrade, python installation)
- [ ] uninstallation and backups of overwritten files
- [ ] fix show hidden files config in nautilus
- [ ] add pdf compression command in bashrc

## Release 240222

First release. Just make things work.

# How to use

```
# copy this folder to tempalte VM
```

and run

```
$ bash run.sh install
```

This installs everything.

# Testing Checklist

Proposed tests after setup scripts were changed.

- [ ] check no sudo permissions on custom files in home folder
- [ ] check font size in terminal
- [ ] check hidden files are displayed in nautilus
- [ ] open inkscape and see if fonts are installed
- [ ] check python installation `$ python` and execute `>>> df_test`. There should appear no error messages at startup.
- [ ] get some overview over installed python packages `$ pip freeze`
- [ ] check hdd usage: `$ df -h`
- [ ] run pycharm `$ pycharm`
- [ ] verify, that anoying text editor tab chaos is switched off
- [ ] check terminal reset shortcut ctrl-w






# qubes-scripts


Collection of scripts and (easy accessible) manuals related with the Qubes Operating System (http://www.qubes-os.org/).

This is a custom script collection for the purposes of our company - not for general purpose.
We made this repo public however for different reasons:
- Feel free to use our scripts, just as they are and stay up to date with this repo.
- Create similar scripts for your usecase and profit from the code we developed already.
- Use this as a (kind of) code based + hands on documentation about some parts of QubesOS.

Each manual / script has a seperate and indenpendent folder, starting with the App VMs name, this is supposed to work for.

All contents are designed to run on a Quebes Fedora APP VMs - if not indicated differntly in the readme.


## Installation
Simply clone the repo.

## Usage
Read the readme in each subfolder, how to use things.

We are just maintinging the most recent versions of each script.
If you encounter a bug or miss a feature in and old version (for instance /fedora35*), chances are given, that this is available in a more recent version (for instance /fedora39*).

## Development
We are continuously developing this further and ajusting the scripts to the latest versions of Qubes.

Plans for the future:

- reducing the amounts of bash sripts and Makefiles to a minimum, having all clean python implementations.

### Current status
With Fedora 35 scripts we did a first proove of concept implementation. Fedora 39 scripts are a bit cleaner - but still far from a professional implementation.


## Support
Support is not yet planed. Contact us by https://www.algorithmus-schmiede.de/.

## Contributing
This is a custom script collection for the purposes of our company - not for general purpose. Never the less, we are happy about external contributions. Feel free to contact us. We will search for a way to include your code.

## Authors and acknowledgment
This project is supplied and maintained by Algorithmus Schmiede.
A software company specialized on data science, numerics and physics.
https://www.algorithmus-schmiede.de/

## License
All installation code is developed by Algorithmus Schmiede, under MIT License (https://en.wikipedia.org/wiki/MIT_License).

We try (hard) to exclude all config files (usually in subfolders data) which might have copy right on them.
In case something slips through, be aware that these files might come with their onw license agreement.
If you want to start a comercial project, do not hesitate to contact us.

Our intention in publishing this repository is to support the Qubes Operating System.
We stongly support all usage of the code supplied here serving this purpose.

## Project status
Will keep this up to date for every (other) fedora template update.

## Source
Source of this repo: https://gitlab.com/algorithmus-schmiede/qubes-scripts

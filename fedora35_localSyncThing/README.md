Installs syncthing locally into home directory, such that AppVM does not need a seperate template / be standalone.

Script is fully implemented in python.
This python code will be the basis for future scripts.

# History and ToDos

Releases are always called according to merge request (MR) / commit / branch name in gitlab repo. 

## ToDos
- [ ] test on fedora 39 and create new folder for this

## Release 240222

First release. Just make things work.
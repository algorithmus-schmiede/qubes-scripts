#!/bin/bash

set -e  # exit on error
set -o xtrace  # print every command before execution


sudo dnf install python pip -y
pip install -r requirements.txt
python3 setup.py install

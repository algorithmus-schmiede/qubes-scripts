# summary

flatpak is able to perform loacl installations and is available in fedora templates already. Use platpak to locally install zoom directly in the AppVM.


# notes


```
flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak search zoom
flatpak install --user Zoom
flatpak run us.zoom.Zoom
```


https://docs.flatpak.org/en/latest/using-flatpak.html#the-flatpak-command

add launcher to dom0 with Command:

```
qvm-run <VM-Name> 'flatpak run us.zoom.Zoom'
```

# update zoom if needed

Sometimes zoom requires an update to work. Do this by

```
flatpak update us.zoom.Zoom
```
#!/bin/bash

FOLDER_DNF_PYTHON="prepared/dnf_python"


set -e  # exit on error
set -o xtrace  # print every command before execution


if python3 --version 1> /dev/null 2>&1; then
  echo "python3 available"  
else
  if ls ${FOLDER_DNF_PYTHON} 1> /dev/null 2>&1; then
    echo "installing python3"
    sudo dnf install "${FOLDER_DNF_PYTHON}/*.rpm"
  else
    echo "python required, but not installed or package available in ${FOLDER_DNF_PYTHON}"
    exit 1
  fi
fi
python3 implementation.py install



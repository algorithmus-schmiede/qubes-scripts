

import pytest

from implementation import slugify


@pytest.mark.parametrize(
    "inp, outp_exp",
    [
        pytest.param("a b c", "a_b_c", id="whitespace -> underscore"),
        pytest.param("https://github.com/tomaspinho/rtl8821ce.git",
                     "https___github_com_tomaspinho_rtl8821ce_git", id="url"),
        pytest.param("a-b c", "a-b_c", id="minus -> underscore"),
    ]
)
def test_slugify(inp, outp_exp):
    assert slugify(inp) == outp_exp
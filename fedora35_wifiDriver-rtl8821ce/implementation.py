import re
import subprocess
from abc import ABC, abstractmethod
from pathlib import Path
from typing import Optional, List
import logging

PATH_PREPARE = Path("prepared")
PATH_DRIVER = PATH_PREPARE / "rtl8821ce"

def logger_default(name=__file__):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s %(name)s l%(lineno)d %(levelname)s:\n\t%(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    return logger

logger = logger_default()

def slugify(str_: str):
    slug = re.sub(r'[^A-z0-9-]', '_', str_)
    return slug

class Task(ABC):

    @property
    @abstractmethod
    def path(self):
        """where to store files for installation (if present)"""
        ...

    @abstractmethod
    def prepare(self):
        """Download files necessary for offline installation"""
        ...

    @abstractmethod
    def install(self):
        ...

class TaskSeperable(Task):
    """
    Task where prepare operation and install operation are independent.
    """
    def install(self):
        if not self.path.is_dir():
            self.prepare()
        self._install()

    @abstractmethod
    def _install(self):
        ...

class GitRepo(TaskSeperable):

    def __init__(self, url: str):
        self.url = url

    @property
    def path(self):
        slug = slugify(self.url)
        return PATH_PREPARE / f"git_{slug}"

    def prepare(self):
        if not self.path.is_dir():
            exit_code = subprocess.call(f"git clone {self.url} {str(self.path)}", shell=True)
            return exit_code
        return None


class GitRepoWithInstallerScript(GitRepo):

    def __init__(self, url: str, rel_path_to_exec: str):
        self.rel_path_to_exec = rel_path_to_exec
        super().__init__(url=url)

    def _install(self):
        path_exec = self.path / self.rel_path_to_exec
        cmd = f"sudo bash {str(self.rel_path_to_exec)}"
        cwd = str(path_exec.parent)
        logger.debug(f"executing: {cmd} in {cwd}")
        exit_code = subprocess.call(cmd, shell=True, cwd=cwd)
        return exit_code

class DnfPackages(Task):

    def __init__(self, packages: List[str]):
        self.pkgs = packages

    @property
    def path(self):
        pkgs_str = slugify("_".join(self.pkgs))
        return PATH_PREPARE / f"dnf_{pkgs_str}"

    def prepare(self):
        self.path.mkdir(exist_ok=True)
        cmd = f"dnf download {' '.join(self.pkgs)} --resolve"
        cwd = self.path
        logger.debug(f"executing: {cmd} in {cwd}")
        exit_code = subprocess.call(cmd, shell=True, cwd=cwd)
        return exit_code

    def install(self):
        cmd = "sudo dnf install "
        if self.path.is_dir():
            cmd += str(self.path / "*.rpm") + " --disablerepo=*"
        else:
            cmd += ' '.join(self.pkgs)
        logger.debug(f"executing: {cmd}")
        exit_code = subprocess.call(cmd, shell=True)
        return exit_code





def basics():
    PATH_PREPARE.mkdir(exist_ok=True)




def prepare(tasks: List[Task]):
    DnfPackages(packages=["python"]).prepare()


    for task in tasks:
        task.prepare()

def install(tasks: List[Task]):
    for task in tasks:
        task.install()

def dev():
    """Executed code here for development purposes only"""
    pass


def main(mode: str):
    basics()

    tasks = [
        GitRepoWithInstallerScript(url="https://github.com/tomaspinho/rtl8821ce.git", rel_path_to_exec="dkms-install.sh"),
    DnfPackages(packages=["dkms", "make", "kernel-devel"]),
    ]

    logger.info(f"Starting mode {mode}")

    if mode == "prepare":
        prepare(tasks=tasks)
    elif mode == "install":
        install(tasks=tasks)
    elif mode == "dev":
        dev()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("mode", choices=["prepare", "install", "dev"], help="prepare: just download data; install: install")
    args = parser.parse_args()
    main(args.mode)
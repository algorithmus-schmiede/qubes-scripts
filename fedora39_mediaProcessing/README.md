# summary

flatpak is able to perform loacl installations and is available in fedora templates already. Use platpak to locally install apps directly in the AppVM.


# after every restart

```
sudo dnf install -y ffmpeg-free  # till fedora template updated
```


# standard apps: kdenlive, audacity, ...


```
# install kdenlive: https://flathub.org/apps/org.kde.kdenlive

flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak remote-list --show-details
flatpak search Kdenlive
flatpak install --user Kdenlive
flatpak run org.kde.kdenlive

flatpak install --user audacity
flatpak run  org.audacityteam.Audacity
flatpak install --user Recorder
flatpak run org.gnome.SoundRecorder

flatpak install --user org.kde.subtitlecomposer
flatpak run org.kde.subtitlecomposer

```


https://docs.flatpak.org/en/latest/using-flatpak.html#the-flatpak-command

add launcher to dom0 with Command:

```
qvm-run <VM-Name> 'flatpak run <FLATPAK_APP>'
```

# Speech to captions

How to create captions / subtitle file for video.

Link to whisper project: https://github.com/openai/whisper

## installation

```
mkdir ~/tmp  # used for unpacking large pip packages
TMPDIR=~/tmp pip install openai-whisper
```

# usage

large-v3 model is the best, obviosly. But takes also pretty long (approx. 4 times longer then video on ryzen3950x, 10 CPUs)


```
# generate subtitles (.srt)
whisper *.mp4 --language de --output_format all --model tiny --output_dir tiny
whisper *.mp4 --language de --output_format all --model medium --output_dir medium
whisper *.mp4 --language de --output_format all --model large-v3 --output_dir large-v3
```





# Speech to subtitles/captions with VOSK in kdenlive

Result is okay. Preferable generate subtitles file seperately and upload with video on youtube / linkedin. This is better for several resons:

- youtube / linkedin knows about subtitles and prefers video
- subtitles can be translated
- subtitles can be switched on/off and changed in size by users

In kdenlive use:
- Settings -> Configure Kdenlive -> Speech to Text -> custom model folder -> set to /home/user/media-proc/kdenlive/vosk_models
- Project -> Subtitles -> Edit Subtitles Tool
- Project -> Subtitles -> Speech recognition -> choose vosk-model-de-0.21

In case you encounter problems, check vosk directly:

```
vosk-transcriber -i VID_20240422_090242.mp4
vosk-transcriber -m ~/media-proc/kdenlive/vosk_models/vosk-model-de-0.21 -i VID_20240422_090242.mp4 -o transcription.txt
```
If things to not work this way, they will not work in kdenlive - obviously ;).
Try different models. It might be, that some work and some don't. Compare: https://github.com/alphacep/vosk-api/issues/991

In our specific case (fedora 39, 16 GB Ram, vos==0.3.45) 3 models from https://alphacephei.com/vosk/models were tested
```
vosk-model-de-0.21  # worked
vosk-model-de-tuda-0.6-900k  # failed
vosk-model-small-de-0.15  # failed
```


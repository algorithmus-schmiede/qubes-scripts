# summary

To create a disposible machine for printing, we need to activate cups service and configure the printer.
As cups configuration need sudo permissions, we have to work with an intermediate template:
- f39-print (template VMs)
- f39-print-dvm (App VM with disposible template option on)

# notes

The first attempt to setup the printer was by installing "system-config-printer".
I was able to setup the printer, but cound never print.
Jobs were stuck in the queue and in properties there was some error
"Printer State: Stopped - cfFilterChain: ghostscript (PID xxx) exited with no errors."

# manual

## setup f39-print

add cups service to qube (this is done in dom0).

start cups and check its running
```
$ sudo systemctl status cups
$ sudo systemctl enable cups
$ sudo systemctl start cups
$ sudo systemctl status cups
```

status if everything is fine (i.e. up and running)
```
[user@f39print ~]$ sudo systemctl status cups
● cups.service - CUPS Scheduler
     Loaded: loaded (/usr/lib/systemd/system/cups.service; enabled; preset: disabled)
    Drop-In: /usr/lib/systemd/system/service.d
             └─10-timeout-abort.conf
             /usr/lib/systemd/system/cups.service.d
             └─30_qubes.conf
     Active: active (running) since Thu 2024-03-07 18:00:16 CET; 2s ago
TriggeredBy: ● cups.path
             ● cups.socket
       Docs: man:cupsd(8)
    Process: 1280 ExecStartPost=/usr/lib/qubes/init/control-printer-icon.sh (code=exited, st>
   Main PID: 1279 (cupsd)
     Status: "Scheduler is running..."
      Tasks: 1 (limit: 385)
     Memory: 3.3M
        CPU: 18ms
     CGroup: /system.slice/cups.service
             └─1279 /usr/sbin/cupsd -l
```


needed for admin access to cups:
```
$ sudo dnf install passwd
$ sudo passwd
# choose onetime-password
```

configure cups:
```
$ sudo firefox &
http://127.0.0.1:631/
# login to admin area via: usr root, pw onetime-password
# add printer -> "Internet Printing Protocol (ipp)" -> Connection: "ipp://192.168.10.24"
#	Name: iP7250
#	Description: Canon PIXMA iP7250
#	Canon -> Canon PIXMA iP7250 - CUPS+Gutenprint v5.3.4 Simplified (en)
#	add printe -> set printer options -> Media Size: A4, 2-Sided Printing: LOng Edge(Standard)
```

## setup f39-print-dvm

- create APP VM and activate disposible template option


## print
- choose `f39-print-dvm` as disposible template in your APP VM
- right click on some pdf -> open in Disp VM
- a pdf viewer in a disposible VM opens: you will find your printer in the print options
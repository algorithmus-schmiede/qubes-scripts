# How to setup a new qubes template

Manual of how to setting up a new qubes template with some standrad packages installed and a few common configurations adjusted.

The following is done:

- install common linux tools (htop, gparted, some pdf viewers, ...)
- install inkscape + gimp
- install libreoffice + german spellchecking
- install latex
- install python with a large custom set of libraries
- install pycharm
- add files to default home directory created if new VMs are based on this template
- install custom fonts

# Preconditions

- Template VM based on Fedora 35, with

```
	sudo qubes-dom0-update qubes-template-fedora-35
```

- private storage: 2 GB
- system storage: 18 GB

Assigning more storage to the TemplateVM as the minimal, does not lead to increased backup size. But never the less, storage can not be decreased in the qubes manager.

# History and ToDos

Releases are always called according to merge request (MR) / commit / branch name in gitlab repo. 

## ToDos

- [ ] add functionality to download all links, dnf + pip packages beforehand (prepare step) for offline installation.
  When installing on TemplateVM, check if downloaded data is available otherwise download
- [ ] makefile to handle prepare, install and test commands

## Release 240222

First release. Just make things work.

# How to use

```
$ bash run.sh prepare
```

Downloads data (pycharm) if necessary. If you have already a pycharm folder in ./data, nothing is downloaded.

```
# copy this folder to tempalte VM
```

and run

```
$ bash run.sh install
```

This installs everything.








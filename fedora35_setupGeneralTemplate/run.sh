#!/bin/bash

# ADJUST THIS SECTION
# ============================================================================================================
DOWNLOADLINK_PYCHARM="https://download.jetbrains.com/python/pycharm-community-2023.3.3.tar.gz"
# ============================================================================================================


set -e  # exit on error
shopt -s extglob  # pattern list matching, https://www.linuxjournal.com/content/bash-extended-globbing
set -o xtrace  # print every command before execution



FOLDER_DATA="data"
FOLDER_HOME_TEMPLATE="/etc/skel"  # https://www.qubes-os.org/doc/templates/



prepare_pycharm () {
  # downloads pycharm if needed
  # otherwise it takes the folder already there (which might/will contain already some customizations)
  if ls ${FOLDER_DATA}/*pycharm-community* 1> /dev/null 2>&1; then
    echo "pycharm already prepared"
  else
    echo "downloading pycharm"
    wget ${DOWNLOADLINK_PYCHARM} -P ${FOLDER_DATA}
    tar -xf ${FOLDER_DATA}/*pycharm*.tar.gz --directory ${FOLDER_DATA}
    rm ${FOLDER_DATA}/*pycharm*.tar.gz
  fi
}



install_pycharm () {
  # PRECONDITION: COPY OF HOME FOLDER - otherwise does the pycharm alias not work
  # just installs pycharm and adds alias to .bashrc
  # pycharm configs have to be set in every AppVM (not yet implemented)
  # some of the configs are stored in /home/user/.config/JetBrains/PyCharmCE2023.3/options/editor.xml, other.xml
  # WARNING: /home/user/.config/JetBrains/PyCharmCE2023.3/* contains also open projects and other VM custom data
  FOLDER_PYCHARM=(`ls -d ${FOLDER_DATA}/*pycharm-community*`)  # array of len 1, example (/usr/lib/python3.10/site-packages)
  if [[ ${#FOLDER_PYCHARM[@]} -eq 1 ]]
  then
    echo "good: ${FOLDER_PYCHARM[0]}"
    sudo rm -rf /opt/*pycharm*
    sudo cp -r ${FOLDER_PYCHARM[0]} /opt
    EXEC_PYCHARM=(`ls -d /opt/*pycharm-community*/bin/pycharm.sh`)
    if [[ ! ${#EXEC_PYCHARM[@]} -eq 1 ]]
    then
      echo "Could not identify pycharm executable: ${EXEC_PYCHARM[@]}"
      exit 1
    fi 
    echo "alias pycharm=\"bash ${EXEC_PYCHARM[0]}\"" | sudo tee -a "${FOLDER_HOME_TEMPLATE}/.bashrc"
  else
    echo "Pycharm folder not found. Call \"bash run.sh prepare\" first: ${PIP_TARGET[@]}"
  fi
  
  # create config files in home dir - NOT WORKING, NOT TESTED, LATEST PROBLEM: PYCHARM POPUP LICENSE AGREEMENTS
  #bash /opt/pycharm-community-2023.3.3/bin/pycharm.sh > /dev/null 2>&1  & PID_PYCHARM=$!
  #sleep 15
  #kill ${PID_PYCHARM}
  #mkdir -p "${FOLDER_HOME_TEMPLATE}/.config/JetBrains"
  #cp -r /home/user/.config/JetBrains "${FOLDER_HOME_TEMPLATE}/.config/JetBrains"
  #CONFIGS_PYCHARM=(`ls -d ${FOLDER_HOME_TEMPLATE}/.config/JetBrains/*pycharm*`)
  #if [[ ! ${#CONFIGS_PYCHARM[@]} -eq 1 ]]
  #then
  #  echo "Could not identify pycharm configs: ${CONFIGS_PYCHARM[@]}"
  #  exit 1
  #fi
  #cp ${FOLDER_DATA}/pycharm_configs/editor.xml "${CONFIGS_PYCHARM[0]}/options"
  #cp ${FOLDER_DATA}/pycharm_configs/editor.xml "${CONFIGS_PYCHARM[0]}/options"
}

available_hdd () {
  df -h
}

copy_home_folder () {
  sudo cp -r ${FOLDER_DATA}/home_template/. ${FOLDER_HOME_TEMPLATE}
  sudo cp -r ${FOLDER_DATA}/home_template/. /home/user
  echo "copied home folder"
}

copy_fonts () {
  sudo cp -rf ${FOLDER_DATA}/fonts/* /usr/share/fonts/ | true  # | true: ignore if font folder is empty
  echo "copied fonts"
}

install_via_dnf () {
  sudo dnf install -y htop g++ gparted  # sys tools
  sudo dnf install -y okular xournal ghostwriter  # pdf, markdown
  sudo dnf install -y audacious # audio player, eventually install some more audacius-* packages
  sudo dnf install -y inkscape gimp # image processing
  sudo dnf install -y libreoffice aspell-de hunspell-de  # office + spell checking
  sudo dnf install -y texlive-scheme-full
  
  

}

install_python () {  
  # install pyhton packages. Per default pip installs into home folder.
  # This does not make sense for a templeate VM. Hence we install into /usr/lib/python*/site-packages
  sudo dnf install -y python3 python3-pip
  sudo pip install --upgrade numpy --target=/usr/lib64/python3.10/site-packages/  # upgrade preinstalled numpy
  PIP_TARGET=(`ls -d /usr/lib/python3*/site-packages`)  # array of len 1, example (/usr/lib/python3.10/site-packages)
  if [[ ${#PIP_TARGET[@]} -eq 1 ]]
  then
    echo "PIP_TARGET: ${PIP_TARGET[0]}"
    FOLDER_PIP_TMP="/var/tmp"  # part of system storage
    #sudo TMPDIR=tmp pip install -r ${FOLDER_DATA}/python_requirements_basics.txt --cache-dir=tmp --target="${PIP_TARGET[0]}"
    #TMPDIR=tmp pip install -r ${FOLDER_DATA}/python_requirements_ml.txt --cache-dir=tmp --build tmp --target="${PIP_TARGET[0]}"
    sudo TMPDIR=$FOLDER_PIP_TMP pip install --upgrade --force-reinstall -r data/python_requirements.txt --cache-dir=$FOLDER_PIP_TMP --target="${PIP_TARGET[0]}"
  else
    echo "Can not run pip install. Found multiple target directories: ${PIP_TARGET[@]}"
  fi
  pip freeze
}

configure_dconf () {
  # use dconf-editor to experiment with propper commands
  # $ sudo dnf install dconf-editor

  # terminal: increase font size
  dconf write /org/gnome/terminal/legacy/profiles:/:$(gsettings get org.gnome.Terminal.ProfilesList default  | tr -d "'")/use-system-font false
  
  # nautilus: adjust view settings
  dconf write /org/gtk/settings/file-chooser/show-hidden true
  dconf write /org/gnome/nautilus/preferences/default-folder-viewer "'list-view'"
  dconf write /org/gnome/nautilus/list-view/default-zoom-level "'small'"
  
  # copy config file
  sudo mkdir -p ${FOLDER_HOME_TEMPLATE}/.config/dconf
  sudo cp -r /home/user/.config/dconf/user ${FOLDER_HOME_TEMPLATE}/.config/dconf/user
}


# script execution
if [ $# -eq 1 ]
then
    if [[ ! ${1} =~ ^(prepare|install|test)$ ]]
    then
      echo "wrong argument \"${1}\". Call this script with either \"bash run.sh perpare\" or \"bash run.sh install\""
      exit 1
    fi
    if [[ ${1} == "test" ]]  # for development purposes only
    then
      echo "Test mode: just for development of this script."
      install_python
    fi
    if [[ ${1} == "prepare" ]]
    then
      echo "Prepare mode: just downloading everything needed. You can manually adjust those downloads then before copy to + exec in template VM."
      prepare_pycharm
    fi
    if [[ ${1} == "install" ]]
    then
      echo "Install mode: setting up a template VM (run this in the template VM)"
      copy_home_folder
      copy_fonts
      available_hdd
      install_via_dnf
      available_hdd
      install_python
      available_hdd
      install_pycharm
      available_hdd
      configure_dconf
      available_hdd
      echo "finished setting up template VM"
    fi
else
    echo "command line argument missing. Call this script with either \"bash run.sh perpare\" or \"bash run.sh install\"."
    exit 1
fi

# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd


# test variables

arr_test = np.arange(20).reshape(5,4).astype(float)
df_test = pd.DataFrame(arr_test, columns=list('abcd'))

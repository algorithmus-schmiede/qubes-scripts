# install qubes
Manual, how to download latest qubes iso, verify, create bootable usb stick

# download iso
download from https://www.qubes-os.org/downloads/
- iso file (Qubes-R4.2.0-x86_64.iso)
- Detached PGPG signature (Qubes-R4.2.0-x86_64.iso.asc)
- Qubes Relese signing key (qubes-release-4.2-signing-key.asc)

# verify chain of trust of downloaded iso.

In short:

- check Qubes-R4.2.0-x86_64.iso.asc was signed with qubes-release-4.2-signing-key.asc
- check Qubes-R4.2.0-x86_64.iso was signed with Qubes-R4.2.0-x86_64.iso.asc
- check qubes Qubes Master Signing Key fingerprint (427F11FD0FAA4B080123F01CDDFA1A3E36879494) on other device

Command line verification:

```
$ gpg2 --no-default-keyring --primary-keyring qubes.gpg --import /usr/share/qubes/qubes-master-key.asc 
gpg: keybox '/home/user/.gnupg/qubes.gpg' created
gpg: key DDFA1A3E36879494: public key "Qubes Master Signing Key" imported
gpg: Total number processed: 1
gpg:               imported: 1

gpg2 --no-default-keyring --primary-keyring qubes.gpg --import qubes-release-4.2-signing-key.asc 
gpg: key E022E58F8E34D89F: public key "Qubes OS Release 4.2 Signing Key" imported
gpg: Total number processed: 1
gpg:               imported: 1
gpg: no ultimately trusted keys found

$ gpg2 --no-default-keyring --primary-keyring qubes.gpg --list-signatures
/home/user/.gnupg/qubes.gpg
---------------------------
pub   rsa4096 2010-04-01 [SC]
      427F11FD0FAA4B080123F01CDDFA1A3E36879494
uid           [ unknown] Qubes Master Signing Key
sig 3        DDFA1A3E36879494 2021-11-29  Qubes Master Signing Key

pub   rsa4096 2022-10-04 [SC]
      9C884DF3F81064A569A4A9FAE022E58F8E34D89F
uid           [ unknown] Qubes OS Release 4.2 Signing Key
sig 3        E022E58F8E34D89F 2022-10-04  Qubes OS Release 4.2 Signing Key
sig          DDFA1A3E36879494 2023-06-03  Qubes Master Signing Key

$ echo "427F11FD0FAA4B080123F01CDDFA1A3E36879494:6:" | gpg --import-ownertrust
gpg: inserting ownertrust of 6

$ gpg2 --no-default-keyring --primary-keyring qubes.gpg --list-signatures
gpg: checking the trustdb
gpg: marginals needed: 3  completes needed: 1  trust model: pgp
gpg: depth: 0  valid:   1  signed:   1  trust: 0-, 0q, 0n, 0m, 0f, 1u
gpg: depth: 1  valid:   1  signed:   0  trust: 1-, 0q, 0n, 0m, 0f, 0u
/home/user/.gnupg/qubes.gpg
---------------------------
pub   rsa4096 2010-04-01 [SC]
      427F11FD0FAA4B080123F01CDDFA1A3E36879494
uid           [ultimate] Qubes Master Signing Key
sig 3        DDFA1A3E36879494 2021-11-29  Qubes Master Signing Key

pub   rsa4096 2022-10-04 [SC]
      9C884DF3F81064A569A4A9FAE022E58F8E34D89F
uid           [  full  ] Qubes OS Release 4.2 Signing Key
sig 3        E022E58F8E34D89F 2022-10-04  Qubes OS Release 4.2 Signing Key
sig          DDFA1A3E36879494 2023-06-03  Qubes Master Signing Key

$ gpg2 --no-default-keyring --primary-keyring qubes.gpg --check-signatures E022E58F8E34D89F
pub   rsa4096 2022-10-04 [SC]
      9C884DF3F81064A569A4A9FAE022E58F8E34D89F
uid           [ unknown] Qubes OS Release 4.2 Signing Key
sig!3        E022E58F8E34D89F 2022-10-04  Qubes OS Release 4.2 Signing Key
sig!         DDFA1A3E36879494 2023-06-03  Qubes Master Signing Key

$ gpg2 --no-default-keyring --primary-keyring qubes.gpg --verify Qubes-R4.2.0-x86_64.iso.asc Qubes-R4.2.0-x86_64.iso
gpg: Signature made Sun Dec 17 19:34:24 2023 CET
gpg:                using RSA key 9C884DF3F81064A569A4A9FAE022E58F8E34D89F
gpg: Good signature from "Qubes OS Release 4.2 Signing Key" [full]
```

# write qubes iso to bootable usb stick:

- Check with lsblk the mountingpoint of the usb stick.
- Then write image with dd.
- Make sure you are writing to device and not the partion (using /dev/sda instead of /dev/sda1)

```
$ lsblk
NAME    MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
xvda    202:0    1 18.8G  0 disk 
├─xvda1 202:1    1  200M  0 part 
├─xvda2 202:2    1    2M  0 part 
└─xvda3 202:3    1 18.6G  0 part /
xvdb    202:16   1 48.9G  0 disk /rw
xvdc    202:32   1   10G  0 disk 
├─xvdc1 202:33   1    1G  0 part [SWAP]
└─xvdc3 202:35   1    9G  0 part 
xvdd    202:48   1    1G  1 disk 

# mount usb stick

[user@downloader Downloads]$ lsblk
NAME    MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda       8:0    1 14.6G  0 disk 
└─sda1    8:1    1 14.6G  0 part 
xvda    202:0    1 18.8G  0 disk 
├─xvda1 202:1    1  200M  0 part 
├─xvda2 202:2    1    2M  0 part 
└─xvda3 202:3    1 18.6G  0 part /
xvdb    202:16   1 48.9G  0 disk /rw
xvdc    202:32   1   10G  0 disk 
├─xvdc1 202:33   1    1G  0 part [SWAP]
└─xvdc3 202:35   1    9G  0 part 
xvdd    202:48   1    1G  1 disk

$ sudo dd if=Qubes-R4.2.0-x86_64.iso of=/dev/sda status=progress bs=1048576 conv=fsync
6513754112 bytes (6.5 GB, 6.1 GiB) copied, 362 s, 18.0 MB/s
6215+1 records in
6215+1 records out
6517848064 bytes (6.5 GB, 6.1 GiB) copied, 425.654 s, 15.3 MB/s
```

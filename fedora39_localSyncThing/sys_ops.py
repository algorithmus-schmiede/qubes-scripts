"""Library for operating system task"""

from typing import Pattern, List

import zipfile

import tarfile

import shutil

import re
import subprocess
from pathlib import Path
import shutils

import requests


def slugify(str_: str):
    slug = re.sub(r"[^A-z0-9-]", "_", str_)
    return slug


def filename_from_download_url(url: str) -> str:
    return re.search("[^/\\&\?]+(?=([\?&].*$|$))", url).group(0)


class SysOps:

    @staticmethod
    def download(url: str, dest: Path):
        """
        Download something from url to destination.

        Parameters
        ----------
        url :
            src to download from
        dest :
            filepath on loacl system

        Returns
        -------

        """
        req = requests.get(url=url, stream=True)
        # with open(dest, 'wb') as fd:
        with dest.open(mode="wb") as fd:
            for chunk in req.iter_content(chunk_size=128):
                fd.write(chunk)

    # TODO: implement 7z support
    @staticmethod
    def extract(src: Path, dest: Path) -> Path:
        """


        Parameters
        ----------
        src
        dest

        Returns
        -------
        Path :
            path to extracted source
            =dest if archive contains only files
            =dest/subfolder if archive contains single subfolder
        List[Path] :
            all extracted files, paths relaitve to dest
        """
        if tarfile.is_tarfile(src):
            files_extracted = tarfile.open(src).getnames()
        elif zipfile.is_zipfile(src):
            files_extracted = zipfile.ZipFile(src).namelist()
        else:
            raise NotImplementedError(f"Unsopportet achive type for {src}")

        shutil.unpack_archive(filename=str(src), extract_dir=str(dest))

        extraction_paths = {fp.split("/")[0] for fp in files_extracted}
        files_extracted = sorted(Path(fe) for fe in files_extracted)

        if len(extraction_paths) == 1:
            extraction_path = next(iter(extraction_paths))
            return dest / extraction_path, files_extracted
        return dest, files_extracted

    @staticmethod
    def search_filepath(path: Path, pattern: str) -> List[Path]:

        return sorted(list(path.rglob(pattern)))

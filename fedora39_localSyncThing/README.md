Installs syncthing locally into home directory, such that AppVM does not need a seperate template / be standalone.

Script is fully implemented in python.
This python code will be the basis for future scripts.

# Usage

```
bash run_install.sh
```

# History and ToDos

Releases are always called according to merge request (MR) / commit / branch name in gitlab repo. 

## ToDos
- [ ] firewall configs for qubes
- [ ] cleanup python code: currently arguments ["download", "install", "dev"] are partially implemented. Decide, if the download functionality is usefull at all.

## Release 240222

Minor improvements compared to fedora35 script.

## Firewall settings

You can find some syncthing adresses on

- https://status.syncthing.net/
- https://docs.syncthing.net/dev/infrastructure.html

however, we were not able to identify firewall configurations for syncthing to work. Syncthing fails already at the discovery step. Also adding all adresses monitored under https://status.syncthing.net/ did not resolve the problem.

In the syncthing browser menue, check "This Decive" -> "Discovery" and click on "4/5" value (or similar). Here you find information, about unestablished connections to discovery servers.
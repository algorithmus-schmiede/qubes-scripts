from setup import Downloadable, SubsetupDownloadInstall


class TestDownloadable:

    @staticmethod
    def test_downloadable(mocker):

        class MyTask(Downloadable):

            def __init__(self, is_downloaded: bool):
                self._is_downloaded = is_downloaded

            @property
            def is_downloaded(self) -> bool:
                return self._is_downloaded

            def _download(self):
                pass

        spy = mocker.spy(MyTask, MyTask._download.__name__)

        MyTask(is_downloaded=True).download()
        assert spy.call_count == 0

        MyTask(is_downloaded=False).download()
        assert spy.call_count == 1


class TestSubsetupDownloadInstall:
    @staticmethod
    def test_setup_new_class():

        class SubsetupTemplate(SubsetupDownloadInstall):

            def __init__(self):
                pass

            @property
            def is_downloaded(self) -> bool:
                pass

            def _download(self):
                pass

            @property
            def is_installed(self) -> bool:
                pass

            def _install(self):
                pass

        SubsetupTemplate()

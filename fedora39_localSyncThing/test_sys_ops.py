import glob

import re

import subprocess
from pathlib import Path

import pytest
import tempfile

from sys_ops import slugify, SysOps, filename_from_download_url


PATH_TEST_DATA = Path("test_data")


@pytest.mark.parametrize(
    "inp, outp_exp",
    [
        pytest.param("a b c", "a_b_c", id="whitespace -> underscore"),
        pytest.param(
            "https://github.com/tomaspinho/rtl8821ce.git",
            "https___github_com_tomaspinho_rtl8821ce_git",
            id="url",
        ),
        pytest.param("a-b c", "a-b_c", id="minus -> underscore"),
    ],
)
def test_slugify(inp, outp_exp):
    assert slugify(inp) == outp_exp


@pytest.mark.parametrize(
    "url, fn_exp",
    [
        pytest.param(
            "https://github.com//v1.27.3/st-v1.27.3.tar.gz",
            "st-v1.27.3.tar.gz",
            id="normal url",
        ),
        pytest.param(
            "https://github.com/something.tar.gz?asd=fgh",
            "something.tar.gz",
            id="query string",
        ),
    ],
)
def test_filename_from_download_url(url, fn_exp):
    assert filename_from_download_url(url) == fn_exp


class TestSysOps:

    class TestDownload:

        @staticmethod
        def test_minimal():
            with tempfile.TemporaryDirectory() as tmp_dir:
                url = "https://api.github.com/events"
                dest = Path(tmp_dir) / "github_api.json"
                SysOps.download(url=url, dest=dest)

    class TestExtract:
        @staticmethod
        @pytest.mark.parametrize(
            "fn, extr_path_exp, n_extr_exp",
            [
                pytest.param("test.tar.xz", ".", 2, id="tar"),
                pytest.param(
                    "test_folder.tar.xz", "test_folder", 3, id="tar with subfolder"
                ),
                pytest.param("test.zip", ".", 2, id="zip"),
                pytest.param(
                    "test_folder.zip", "test_folder", 3, id="zip with subfolder"
                ),
            ],
        )
        def test_minimal(fn, extr_path_exp, n_extr_exp):
            with tempfile.TemporaryDirectory() as tmp_dir:
                tmp_dir = Path(tmp_dir)
                compr_format = ".".join(fn.split(".")[1:])
                src = PATH_TEST_DATA / fn
                dest = tmp_dir / f"TestExtract_minimal_{compr_format.replace('.', '_')}"
                extr_path, files_extracted = SysOps.extract(src=src, dest=dest)

                assert extr_path == dest / extr_path_exp

                # check if files were extracted
                files_extracted_found = list(
                    extr_path / fp for fp in extr_path.rglob("*")
                )
                if dest != extr_path:
                    files_extracted_found.append(extr_path)
                files_extracted_found = sorted(
                    Path(fp).relative_to(dest) for fp in files_extracted_found
                )
                assert files_extracted == files_extracted_found
                assert len(files_extracted_found) == n_extr_exp

        @staticmethod
        @pytest.mark.parametrize(
            "fn",
            [
                pytest.param("test.7z", id="7z"),
                pytest.param("test_folder.7z", id="7z with subfolder"),
            ],
        )
        def test_unsupportet_type(fn):
            with pytest.raises(NotImplementedError):
                SysOps.extract(
                    src=PATH_TEST_DATA / fn, dest=tempfile.TemporaryDirectory()
                )

    class TestPathFromRegex:
        @staticmethod
        @pytest.mark.parametrize(
            "path, pattern, fps_exp",
            [
                pytest.param(
                    PATH_TEST_DATA,
                    "*1.txt",
                    [
                        Path("test_data/test1.txt"),
                        Path("test_data/test_folder/test1.txt"),
                    ],
                ),
                pytest.param(PATH_TEST_DATA, "1.txt", []),
                pytest.param(
                    PATH_TEST_DATA.absolute(),
                    "test1.txt",
                    [
                        PATH_TEST_DATA.absolute() / fn
                        for fn in ["test1.txt", "test_folder/test1.txt"]
                    ],
                ),
            ],
        )
        def test_minimal(path, pattern, fps_exp):

            fps = SysOps.search_filepath(path=path, pattern=pattern)
            assert fps == fps_exp

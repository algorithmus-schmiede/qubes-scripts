import re

from abc import ABC, abstractmethod
from pathlib import Path
from typing import Optional, List, Union
import logging

from sys_ops import filename_from_download_url, SysOps

PATH_TMP_DOWNLOADS = Path("tmp_downloaded")
PATH_DATA = Path("data")
PATH_AUTOSTART = Path.home() / ".config/autostart"


def logger_default(name=__file__):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter(
        "%(asctime)s %(name)s l%(lineno)d %(levelname)s:\n\t%(message)s"
    )
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    return logger


logger = logger_default()


class Downloadable(ABC):

    @property
    @abstractmethod
    def is_downloaded(self) -> bool:
        """check if download step was performed"""
        ...

    def download(self):
        """Download files necessary for offline installation"""
        if self.is_downloaded:
            logger.info("Subsetup was downloaded already")
            return
        logger.info("Downloading subsetup")
        self._download()

    @abstractmethod
    def _download(self): ...


# TODO class DownloadableSkip(Downloadable): not sure if I will need that


class Installable(ABC):

    @property
    @abstractmethod
    def is_installed(self) -> bool:
        """check if install step was performed"""
        ...

    def install(self):
        if self.is_installed:
            logger.info("Subsetup was installed already")
            return
        logger.info("Installing subsetup")
        self._install()

    @abstractmethod
    def _install(self): ...


class SubsetupDownloadInstall(Downloadable, Installable):

    def install(self):
        if self.is_installed:
            logger.info("Subsetup was installed already")
            return
        if not self.is_downloaded:
            Downloadable.download(self)
        else:
            logger.info("Subsetup was downloaded already")
        Installable.install(self)


class SubsetupDownloadExtractAutostart(SubsetupDownloadInstall):
    """
    Attributes
    ----------

    url : str
        Download url
    path_installation: Path
        where downloaded archive is extracted to
    rel_path_exec : Path
        relative path to executable in downloaded + extracted folder
    name : str
        human understandable name for this Subsetup
    """

    def __init__(
        self,
        url: str,
        path_installation: Path,
        rel_path_exec: Union[Path, str],
        name: str
    ):
        """

        Parameters
        ----------
        fp_download_compr :
            where to download zip/tar.gz to
        fp_extraction_target :
            where to extract downloaded archive
        cmd_exec :
            creates a file /home/user/.config/autostart/....desktop
        name :
            name for autostart file
        rel_path_exec:
            Path: relative path name
            str: search pattern sued by Path.rglob
        """
        self.url = url
        self.path_installation = path_installation / name
        self.rel_path_exec = rel_path_exec

        fn_download_compr = filename_from_download_url(url)
        self.name = name

        fp_download_compr = PATH_TMP_DOWNLOADS / fn_download_compr
        self.fp_download_compr = fp_download_compr

    @property
    def is_downloaded(self) -> bool:
        return self.fp_download_compr.exists()

    def _download(self):
        return SysOps.download(url=self.url, dest=self.fp_download_compr)

    @property
    def is_installed(self) -> bool:
        if not self.path_installation.exists():
            return False
        if not (PATH_AUTOSTART / f"{self.name}.desktop").exists():
            return False
        return True

    def _install(self):
        # extract
        self.path_installation.mkdir(exist_ok=True, parents=True)
        fp_extract, fps_extracted = SysOps.extract(
            src=self.fp_download_compr, dest=self.path_installation
        )

        # fp_extract = (PATH_DATA / "autostartTemplate.desktop").read_text()
        fp_autostart_script = SysOps.search_filepath(
            path=self.path_installation,
            pattern="syncthing-linux-amd64-v*/etc/linux-desktop/syncthing-start.desktop",
        )
        assert len(fp_autostart_script) == 1
        entry_autostart = fp_autostart_script[0].read_text()
        if isinstance(self.rel_path_exec, Path):
            path_exec = self.path_installation / self.rel_path_exec
        else:
            path_exec = SysOps.search_filepath(
                path=self.path_installation, pattern=self.rel_path_exec
            )
            assert len(path_exec) == 1
            path_exec = self.path_installation / path_exec[0]
        entry_autostart = re.sub("\nExec=(\S*)", f"\nExec={path_exec}", entry_autostart)
        PATH_AUTOSTART.mkdir(exist_ok=True, parents=True)
        (PATH_AUTOSTART / f"{self.name}.desktop").write_text(entry_autostart)


class SubsetupSyncthingLocalInstall(SubsetupDownloadExtractAutostart):

    def __init__(self):

        super().__init__(
            url="https://github.com/syncthing/syncthing/releases/download/v1.27.3/syncthing-linux-amd64-v1.27.3.tar.gz",
            path_installation=Path.home() / "opt",
            rel_path_exec="syncthing-linux-amd64-v*/syncthing",
            name="syncthing",
        )


def basics():
    PATH_TMP_DOWNLOADS.mkdir(exist_ok=True, parents=True)
    # DnfPackages(packages=["python"]).prepare()  # TODO


def install(tasks: List[Installable]):
    SubsetupSyncthingLocalInstall().install()

    logger.info("Sucessfully installed syncthing. Please restart VM and open 127.0.0.1:8384 in browser")


def main(mode: str):
    basics()

    tasks = []

    logger.info(f"Starting mode {mode}")

    if mode == "install":
        install(tasks=tasks)
    else:
        raise NotImplementedError


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "mode",
        choices=["download", "install", "dev"],
        help="prepare: just download data; install: install",
    )
    args = parser.parse_args()
    main(args.mode)
